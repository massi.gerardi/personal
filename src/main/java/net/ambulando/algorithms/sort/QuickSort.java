package net.ambulando.algorithms.sort;

import static net.ambulando.algorithms.sort.Utils.swap;

public class QuickSort<T extends Comparable<T>> {

  public void sort(T[] array) {
    sort(array, 0, array.length-1);
  }

  private void sort(T[] array, int start, int end) {
    if (start < end) {
      int middle = pivot(array, start, end);
      sort(array, start, middle - 1);
      sort(array, middle + 1, end);
    }
  }

  private int pivot(T[] array, int start, int end) {
    T pivot = array[end];
    int k = start - 1;
    for (int i = start; i < end; i++) {
      if (array[i].compareTo(pivot) < 0) {
        k++;
        swap(array,i,k);
      }
    }
    k++;
    swap(array,end,k);
    return k;
  }

}
