package net.ambulando.algorithms.sort;

import java.util.Arrays;

public class InsertionSort<T extends Comparable<T>> {

  public T[] sort(T[] array) {
    for (int i = 1; i < array.length; i++) {
      shift(array, i);
    }
    return array;
  }

  private void shift(T[] array, int i) {
    T current = array[i];
    int j = i - 1;
    while (j >= 0 && current.compareTo(array[j]) < 0) {
      array[j+1] = array[j];
      array[j] = current;
      j--;
    }
  }


}
