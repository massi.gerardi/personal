package net.ambulando.algorithms.sort;

public class Utils {

  public static <T> void swap(T[] array, int i, int k){
    T temp = array[i];
    array[i] = array[k];
    array[k] = temp;
  }


  public static <T> String toString(T[] array, int start, int middle, int end) {
    StringBuilder builder = new StringBuilder("[");
    for (int i = start; i < middle + 1; i++) {
      builder.append(array[i]).append(",");
    }
    builder.append("]");
    if (end > -1) {
      builder.append("[");
      for (int i = middle + 1; i < end + 1; i++) {
        builder.append(array[i]).append(",");
      }
      builder.append("]");
    }
    return builder.toString();
  }


}
