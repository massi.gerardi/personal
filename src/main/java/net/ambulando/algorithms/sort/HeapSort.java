package net.ambulando.algorithms.sort;

import java.util.Arrays;

import static net.ambulando.algorithms.sort.Utils.swap;

public class HeapSort<T extends Comparable<T>> {

  public void sort(T[] array) {
    for (int i = array.length / 2 - 1; i >= 0; i--)
      heapify(array, array.length, i);
    for (int i = array.length - 1; i >= 0; i--) {
      swap(array, i, 0);
      heapify(array, i, 0);
    }
  }

  private void heapify(T[] array, int length, int i) {
    int large = i;
    int l = 2 * i + 1;
    int r = 2 * i + 2;
    if (l < length && array[l].compareTo(array[large]) > 0)
      large = l;
    if (r < length && array[r].compareTo(array[large]) > 0)
      large = r;
    if (large != i) {
      swap(array, i, large);
      heapify(array, length, large);
    }
  }

}
