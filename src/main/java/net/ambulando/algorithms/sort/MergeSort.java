package net.ambulando.algorithms.sort;

public class MergeSort<T extends Comparable<T>> {

  public void sort(T[] array) {
    sort(array, 0, array.length - 1);
  }

  private void sort(T[] array, int start, int end) {
    if (start < end) {
      int middle = (end + start) / 2;
      sort(array, start, middle);
      sort(array, middle + 1, end);
      merge(array, start, middle, end);
    }
  }

  private void merge(T[] array, int start, int middle, int end) {
    int l = start;
    int r = middle + 1;
    while (l < end  && r < end + 1) {
      if (l < r && array[r].compareTo(array[l]) < 0) { // r < l
        shift(array, l, r);
        r++;
      }
      l++;
    }
  }

  private void shift(T[] array, int l, int r) {
    T value = array[r];
    for (int i = l; i < r + 1; i++) {
      T temp = array[i];
      array[i] = value;
      value = temp;
    }
  }


}
