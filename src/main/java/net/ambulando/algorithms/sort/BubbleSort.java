package net.ambulando.algorithms.sort;

import java.util.Arrays;

public class BubbleSort<T extends Comparable<T>> {

  public T[] sort(T[] array) {
    for (int j = 0; j<array.length - 1; j++) {
      for (int i = 0; i < array.length - j - 1; i++) {
        if (array[i].compareTo(array[i + 1]) > 0) {
          T temp = array[i];
          array[i] = array[i + 1];
          array[i + 1] = temp;
        }
      }
    }
    return array;
  }
}
