package net.ambulando.algorithms.sort;

public class SelectionSort<T extends Comparable<T>> {

  public void sort(T[] array) {
    for (int i = 0; i < array.length-1; i++) {
      int min = getMin(array, i, array.length);
      T temp = array[min];
      array[min] = array[i];
      array[i] = temp;
    }
  }

  private int getMin(T[] array, int start, int end) {
    int min = start;
    for (int i = start; i < end; i++) {
      if (array[i].compareTo(array[min]) < 0) min = i;
    }
    return min;
  }

  public static class BubbleSort<T extends Comparable<T>> {

    public T[] sort(T[] array) {
      for (int i = 0; i<array.length - 1; i++) {
        if (array[i].compareTo(array[i+1]) < 0) {
          T temp = array[i];
          array[i] = array[i+1];
          array[i+1] = temp;
        }
      }
      return array;
    }
  }
}
