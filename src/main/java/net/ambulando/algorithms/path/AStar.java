package net.ambulando.algorithms.path;

import java.util.PriorityQueue;
import java.util.Queue;

public class AStar {

  Queue<Node> queue = new PriorityQueue<Node>();

  public Node path(Node start, Node end){
    queue.add(start);
    while (!queue.isEmpty()) {
      Node node = queue.poll();
      if (node.id.equals(end.id)) return end;
      

    }
    return null;
  }

}
