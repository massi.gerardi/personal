package net.ambulando.algorithms.path;

import java.util.PriorityQueue;
import java.util.Queue;

public class Dijkstra {

  Queue<Node> visited = new PriorityQueue<Node>();

  public Node path(Node start, Node end) {
    visited.add(start);
    start.distance = 0;
    while (!visited.isEmpty()) {
      Node node = visited.poll();
      visit(node);
      if (end.visited) return end;
    }
    return null;
  }

  private void visit(Node node) {
    System.out.println("visiting "+node);
    node.visited = true;
    for (Node near : node.near) {
      if (near.visited) continue;
      if (node.distance + node.distance(node) < near.distance) {
        near.distance = node.distance + node.distance(near);
        near.previous = node;
        visited.add(near);
      }

    }
  }


}
