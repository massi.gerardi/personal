package net.ambulando.algorithms.path;

import java.util.ArrayList;
import java.util.List;

public class Node implements Comparable<Node> {

  String id;
  Integer distance = Integer.MAX_VALUE;
  int weight;
  Node previous;
  boolean visited = false;
  List<Node> near = new ArrayList<Node>();

  public int distance(Node that) {
    return this.weight + that.weight;
  }

  public int compareTo(Node that) {
    return this.distance.compareTo(that.distance);
  }

  @Override
  public String toString() {
    return "["+id+" "+distance+"]";
  }
}
