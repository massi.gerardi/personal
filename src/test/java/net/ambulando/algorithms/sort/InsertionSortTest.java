package net.ambulando.algorithms.sort;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class InsertionSortTest {

  @Test
  public void sortInteger() {
    InsertionSort<Integer> sort = new InsertionSort<Integer>();
    Integer[] array = new Integer[]{64, 20, 35, 11, 24};
    Integer[] expected = new Integer[]{11, 20, 24, 35, 64};
    Integer[] sorted = sort.sort(array);
    Assert.assertArrayEquals(expected, sorted);
  }

  @Test
  public void sortString() {
    InsertionSort<String> sort = new InsertionSort<String>();
    String[] array = new String[]{"one", "two", "three", "four", "five"};
    String[] expected = new String[]{"five", "four", "one", "three", "two"};
    String[] sorted = sort.sort(array);
    Assert.assertArrayEquals(expected, sorted);
  }



}