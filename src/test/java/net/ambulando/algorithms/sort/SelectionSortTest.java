package net.ambulando.algorithms.sort;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class SelectionSortTest {

  @Test
  public void sortInteger() {
    SelectionSort<Integer> sort = new SelectionSort<Integer>();
    Integer[] array = new Integer[]{64, 20, 35, 11, 24};
    Integer[] expected = new Integer[]{11, 20, 24, 35, 64};
    sort.sort(array);
    Assert.assertArrayEquals(expected, array);
  }

  @Test
  public void sortString() {
    SelectionSort<String> sort = new SelectionSort<String>();
    String[] array = new String[]{"one", "two", "three", "four", "five"};
    String[] expected = new String[]{"five", "four", "one", "three", "two"};
    sort.sort(array);
    Assert.assertArrayEquals(expected, array);
  }

}