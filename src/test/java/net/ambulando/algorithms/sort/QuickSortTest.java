package net.ambulando.algorithms.sort;

import org.junit.Assert;
import org.junit.Test;

public class QuickSortTest {

  @Test
  public void sortInteger() {
    QuickSort<Integer> sort = new QuickSort<Integer>();
    Integer[] array =    new Integer[]{64, 20, 35, 11, 24, 100, 300, 1, 25, 24};
    Integer[] expected = new Integer[]{1,  11, 20, 24, 24, 25, 35,  64, 100, 300};
    sort.sort(array);
    Assert.assertArrayEquals(expected, array);
  }

  @Test
  public void sortString() {
    QuickSort<String> sort = new QuickSort<String>();
    String[] array = new String[]{"one", "two", "three", "four", "five"};
    String[] expected = new String[]{"five", "four", "one", "three", "two"};
    sort.sort(array);
    Assert.assertArrayEquals(expected, array);
  }


}