package net.ambulando.algorithms.path;

import org.junit.Assert;
import org.junit.Test;

public class DijkstraTest {

  @Test
  public void testA() {
    Node start = new Node();
    start.id = "start";
    start.weight = 10;
    Node end = new Node();
    end.id = "end";
    end.weight = 10;

    Node node = null;
    node = addNear(start, 1, "11");
    node = addNear(node, 2, "11");
    node = addNear(node, 2, "12");
    node = addNear(node, 3, "13");

    node = addNear(start, 2, "21");
    node = addNear(node, 3, "22");
    node = addNear(node, 3, "23");
    node = addNear(node, 2, "24");
    node.near.add(end);

    node = addNear(start, 2, "31");
    node = addNear(node, 4, "32");
    node = addNear(node, 2, "33");
    node = addNear(node, 4, "34");
    node = addNear(node, 4, "35");
    node.near.add(end);

    Dijkstra dijkstra = new Dijkstra();
    Node result = dijkstra.path(start, end);
    Assert.assertNotNull(result);
    Assert.assertEquals("end", result.id);
    System.out.print(result.id+" ");
    Node p = result.previous;
    while (p != null) {
      System.out.print(p.id+" ");
      p = p.previous;
    }


  }

  @Test
  public void testC() {
    Node start = new Node();
    start.id = "start";
    start.weight = 10;
    Node end = new Node();
    end.id = "end";
    end.weight = 10;

    Node node = null;
    node = addNear(start, 1, "11");
    node = addNear(node, 2, "11");
    node = addNear(node, 2, "12");
    node = addNear(node, 3, "13");

    node = addNear(start, 2, "21");
    node = addNear(node, 3, "22");
    node = addNear(node, 3, "23");
    node = addNear(node, 2, "24");
    node.near.add(end);

    node = addNear(start, 1, "31");
    node = addNear(node, 1, "32");
    node = addNear(node, 1, "33");
    node = addNear(node, 1, "34");
    node = addNear(node, 1, "35");
    node.near.add(end);

    Dijkstra dijkstra = new Dijkstra();
    Node result = dijkstra.path(start, end);
    Assert.assertNotNull(result);
    Assert.assertEquals("end", result.id);
    System.out.print(result.id+" ");
    Node p = result.previous;
    while (p != null) {
      System.out.print(p.id+" ");
      p = p.previous;
    }


  }
  @Test
  public void testB() {
    Node start = new Node();
    start.id = "start";
    start.weight = 10;
    Node end = new Node();
    end.id = "end";
    end.weight = 10;

    Node node = null;
    node = addNear(start, 1, "11");
    node = addNear(node, 2, "11");
    node = addNear(node, 2, "12");
    node = addNear(node, 3, "13");

    node = addNear(start, 2, "21");
    node = addNear(node, 3, "22");
    node = addNear(node, 3, "23");
    node = addNear(node, 2, "24");

    node = addNear(start, 2, "31");
    node = addNear(node, 4, "32");
    node = addNear(node, 2, "33");
    node = addNear(node, 4, "34");
    node = addNear(node, 4, "35");

    Dijkstra dijkstra = new Dijkstra();
    Node result = dijkstra.path(start, end);
    Assert.assertNull(result);

  }

  private Node addNear(Node node, int weight, String id) {
    Node near = new Node();
    near.id = id;
    near.weight = weight;
    node.near.add(near);
    return near;
  }

}